module mimir

go 1.18

require (
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/hashicorp/go-immutable-radix v1.3.0 // indirect
	github.com/hashicorp/go-memdb v1.3.3 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.13 // indirect
)
