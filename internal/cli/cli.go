package cli

import (
	"flag"
	"fmt"
	"io"
	"os"
)

type Option func(*Cli)

type Cli struct {
	output io.Writer
}

type Args struct {
	InFile string
	DbFile string
}

func New(opts ...Option) Cli {
	it := Cli{output: os.Stderr}

	for _, opt := range opts {
		opt(&it)
	}

	return it
}

func (c Cli) ParseArgs(args []string) (Args, error) {
	var out Args

	flags := flag.NewFlagSet(args[0], flag.ContinueOnError)
	flags.SetOutput(c.output)

	flags.StringVar(&out.InFile, "f", "", "path to an input csv file")
	flags.StringVar(&out.DbFile, "o", "db.sqlite", "path to which to write the output db file")

	if err := flags.Parse(args[1:]); err != nil {
		return Args{}, fmt.Errorf("invalid argument: %v", err)
	}

	if missing := validateRequiredFlagsSet(flags, "f"); missing != nil {
		err := fmt.Errorf("missing required flag(s) %+v", missing)

		_, _ = fmt.Fprintln(c.output, err)
		flags.Usage()

		return Args{}, err
	}

	return out, nil
}

func validateRequiredFlagsSet(flags *flag.FlagSet, req ...string) []string {
	set := make(map[string]struct{})
	flags.Visit(func(f *flag.Flag) { set[f.Name] = struct{}{} })

	var missing []string
	for _, r := range req {
		if _, ok := set[r]; !ok {
			missing = append(missing, "-"+r)
		}
	}

	return missing
}
