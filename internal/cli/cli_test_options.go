package cli

import (
	"io"
	"testing"
)

func WithOutput(t testing.TB, output io.Writer) Option {
	t.Helper()

	return func(cli *Cli) {
		cli.output = output
	}
}
