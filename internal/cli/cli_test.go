package cli_test

import (
	"errors"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"mimir/internal/cli"
	"mimir/internal/test"
)

func Test(t *testing.T) {
	const usage = "Usage of test:\n" +
		"  -f string\n    \tpath to an input csv file\n" +
		"  -o string\n    \tpath to which to write the output db file (default \"db.sqlite\")\n"

	t.Parallel()
	tcs := map[string]struct {
		args    []string
		want    cli.Args
		wantErr error
		wantOut string
	}{
		"should fail when input file is missing": {
			wantErr: errors.New("missing required flag(s) [-f]"),
			wantOut: "missing required flag(s) [-f]\n" + usage,
		},
		"should default when db file is missing": {
			args: []string{"-f", "input.file"},
			want: cli.Args{InFile: "input.file", DbFile: "db.sqlite"},
		},
		"should parse valid arguments": {
			args: []string{
				"-f", "input.file",
				"-o", "output.sqlite",
			},
			want: cli.Args{InFile: "input.file", DbFile: "output.sqlite"},
		},
		"should reject invalid arguments": {
			args:    []string{"-nope"},
			wantErr: errors.New("invalid argument: "),
			wantOut: "flag provided but not defined: -nope\n" + usage,
		},
	}
	for name, tc := range tcs {
		tc := tc
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			var out = strings.Builder{}
			c := cli.New(cli.WithOutput(t, &out))
			actual, err := c.ParseArgs(append([]string{"test"}, tc.args...))
			test.AssertErrorStartsWith(t, tc.wantErr, err)

			if diff := cmp.Diff(tc.wantOut, out.String()); diff != "" {
				t.Errorf("usage string mismatch:%s\n", diff)
			}

			if diff := cmp.Diff(tc.want, actual); diff != "" {
				t.Errorf("mismatch:%s\n", diff)
			}
		})
	}
}
