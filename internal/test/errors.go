package test

import (
	"reflect"
	"strings"
	"testing"
)

// AssertErrorStartsWith is a test helper that checks that the error is of the expected type and starts with the expected message
func AssertErrorStartsWith(t *testing.T, expected error, actual error) {
	t.Helper()

	switch {
	case expected == nil && actual != nil:
		t.Errorf("unexpected error: %v", actual)
	case expected != nil && actual == nil:
		t.Errorf("expected error %v but got none", expected)
	case reflect.TypeOf(expected) != reflect.TypeOf(actual):
		fallthrough
	case actual != nil && !strings.HasPrefix(actual.Error(), expected.Error()):
		t.Errorf("error starts with mismatch\nexpected: %[1]T %[1]v\n but got: %[2]T %[2]v", expected, actual)
	}
}
