package main

import (
	"context"
	"database/sql"
	"encoding/csv"
	"fmt"
	"os"
	"strings"

	_ "github.com/mattn/go-sqlite3"
	"mimir/internal/cli"
)

const data = string(
	`name,age,phone
bob,22,"{""area"":""403"", ""num"":""555-5555""}"
jane,25,"{""area"":""444"", ""num"":""333-3333"", ""country"":""+42""}"
tony,26,`,
)

func main() {
	console := cli.New()
	args, err := console.ParseArgs(os.Args)
	if err != nil {
		os.Exit(2)
	}

	db, err := sql.Open("sqlite3", fmt.Sprintf("file:%s?cache=shared", args.DbFile))
	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	conn, err := db.Conn(ctx)
	if err != nil {
		panic(err)
	}

	rdr := csv.NewReader(strings.NewReader(data))
	tbl, err := rdr.ReadAll()
	if err != nil {
		panic(err)
	}

	var header []string
	var rows [][]string

	if len(tbl) > 0 {
		header = tbl[0]
	}

	if len(tbl) > 1 {
		rows = tbl[1:]
	}

	cols := make([]string, len(header))
	for i, s := range header {
		cols[i] = s + " VARCHAR"
	}

	create := fmt.Sprintf("CREATE TABLE person (id INTEGER PRIMARY KEY, %s);", strings.Join(cols, ","))
	if _, err := conn.ExecContext(ctx, create); err != nil {
		panic(err)
	}

	ps := make([]string, len(rows))
	for i, row := range rows {
		strs := make([]string, len(row))
		for j, s := range row {
			strs[j] = fmt.Sprintf("'%s'", s)
		}

		ps[i] = "(" + strings.Join(strs, ",") + ")"
	}

	insert := fmt.Sprintf("INSERT INTO person (%s) VALUES %s;", strings.Join(header, ","), strings.Join(ps, ","))
	if _, err := conn.ExecContext(ctx, insert); err != nil {
		panic(err)
	}

	res1, err := conn.QueryContext(ctx, "SELECT * FROM person")
	if err != nil {
		panic(err)
	}

	for res1.Next() {
		var id, name, age, phone string
		if err := res1.Scan(&id, &name, &age, &phone); err != nil {
			panic(err)
		}

		println("id: ", id, "\tname: ", name, "\tage: ", age, "\tphone: ", phone)
	}

	res2, err := conn.QueryContext(ctx, "SELECT p.name, json_extract(p.phone, '$.area'), json_extract(p.phone, '$.num'), json_extract(p.phone, '$.country') FROM person AS p")
	if err != nil {
		panic(err)
	}

	for res2.Next() {
		var name, area, num, country interface{}
		if err := res2.Scan(&name, &area, &num, &country); err != nil {
			panic(err)
		}

		fmt.Printf("%s: %v (%v) %v\n", name, country, area, num)
	}
}
